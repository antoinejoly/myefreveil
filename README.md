# Librairies utilisées

|Librairie|Version|Description|
|---|---|---|
|Async Storage|1.13.2|Permet de stocker des informations dans l'application|
|Cookies|5.0.1|Utilisation des cookies du navigateur WebView|
|react-navigation|   |   |
|babel-plugin-module-alias|5.8.9|Permet de créer des raccourcis modules|
|react native paper|4.4.0|Composants supplémentaires|
|react-native-webview|10.10.0|Intégration d'un navigateur web InApp|

# Helpers

|Nom|Valeur retournée|Description|
|---|---|---|
|connection_tester|Booléan|Vérifie si l'utilisateur est connecté. Retourne true si connecté sinon false|
|parser|Array|Récupère l'emploi du temps pour récupérer les premiers des journées de la semaine|
|stringtoHours|String|Permet de formater une chaine de caractère: 800 -> 8:00|
|time|Object|Ce helper possède deux fonctions: getWeek pour récupérer le début et la fin de la semaine et addDay pour formater un string et ajouter un jour|

# Écrans

|Nom|Description|Parent|
|---|---|---|
|App|Défini le design de l'application|   |
|MainScreen|Test si l'utilisateur est connecté|App|
|LoginScreen|Écran d'authentification|MainScreen|
|InApp|Écran affiché lorsque l'utilisateur est connecté|MainScreen|
|AlarmsScreen|Affiche les alarmes|InApp|
|SettingsScreen|Affiche les paramètres de l'application|InApp|

Documentation du réveil : https://docs.expo.io/versions/latest/sdk/notifications/
