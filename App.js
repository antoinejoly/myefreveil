import { StatusBar } from 'expo-status-bar';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import MainScreen from 'components/screens/MainScreen';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#0e3b5a',
    accent: '#f1c40f',
  },
};
export default function App() {
  return(
    <View style={styles.container}>
      <StatusBar style="auto" />
      <PaperProvider theme={ theme }>
        <MainScreen/>
      </PaperProvider>
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: Expo.Constants.statusBarHeight
  }
});
