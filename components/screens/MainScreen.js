import React, { Component } from 'react';
import { StyleSheet, View, TextView, StatusBar } from "react-native";
import {connection_tester} from 'helpers/connection_tester';
import LoginScreen from 'components/screens/LoginScreen';
import InApp from 'components/screens/InApp';
import * as SplashScreen from 'expo-splash-screen';
import { Text } from 'react-native-paper';

class MainScreen extends Component {

  constructor (props) {
    super(props);
    this.setIsLogged = this.setIsLogged.bind(this);
  }

  state = {
    isLogged: false,
    testConnection: false
  }

  async componentDidMount() {
    await SplashScreen.preventAutoHideAsync();
    let connection_status = await connection_tester()
    if(connection_status==true) {
      this.setState({ isLogged : true })
    }
    this.setState({ testConnection : true })
    await SplashScreen.hideAsync()
  }
  
  async setIsLogged(){
    let connection_status = await connection_tester()
    if(connection_status==true) {
      this.setState({ isLogged : true })
    } else {
      this.setState({ isLogged : false })
    }
  }

  render() {
    if(this.state.testConnection) {
      if(this.state.isLogged) {
        screen=<InApp setIsLogged={this.setIsLogged}/>
      } else {
        screen=<LoginScreen setIsLogged={this.setIsLogged}/>
      }
    } else {
      screen=<Text>Loading</Text>
    }

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#001530" barStyle="light-content"/>
        {screen}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
   }
});

export default MainScreen;