import React, { Component } from 'react';
import { Text, List, Switch } from 'react-native-paper';
import { StyleSheet } from "react-native";
import { View } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { update_notifications } from 'helpers/updateNotifications';

class SettingsScreen extends Component {

  state = {
    disabledAlarms: false,
    isTimePickerVisible: false,
    offset: new Date(2020, 1, 1, 0, 30),
  }

  async componentDidMount() {
    var disabledAlarms = await AsyncStorage.getItem('@disabledAlarms')
    var offset = await AsyncStorage.getItem('@offset')
    if(disabledAlarms) {
      this.setState({disabledAlarms: JSON.parse(disabledAlarms)})
    }
    if(offset) {
      this.setState({offset: new Date(JSON.parse(offset))})
    }
  }

  changeDisabledAlarms = async () => {
    var tmpdata=!this.state.disabledAlarms
    await AsyncStorage.setItem('@disabledAlarms', JSON.stringify(tmpdata))
    this.setState({disabledAlarms: tmpdata})
    update_notifications()
  }

  handleConfirm = async (event, date) => {
    this.setState({isTimePickerVisible: false});
    if(date !== undefined) {
      await AsyncStorage.setItem('@offset', JSON.stringify(date))
      this.setState({offset: date})
      update_notifications()
    }
  };
  
  render() {
    return (
      <View>
        <List.Item
          title="Désactiver les alarmes"
          onPress={() => this.changeDisabledAlarms()}
          style={styles.list}
          right={ props =>
            <Switch
              color={'#0e3b5a'}
              value={this.state.disabledAlarms}
              onValueChange={() => this.changeDisabledAlarms()}
            />
          }
        />
        <List.Item
          title="Temps avant réveil"
          style={styles.list}
          onPress={() => this.setState({ isTimePickerVisible: true })}
          right={ props =>
            <Text>{this.state.offset.getHours()}:{this.state.offset.getMinutes()}</Text>
          }
        />
        { this.state.isTimePickerVisible && (<DateTimePicker
        mode="time"
        display="spinner"
        value={this.state.offset}
        onChange={this.handleConfirm}
      />)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(85, 85, 85, .05)',
  }
});

export default SettingsScreen;