import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Appbar, BottomNavigation } from 'react-native-paper';
import AlarmsScreen from 'components/screens/AlarmsScreen'
import SettingsScreen from 'components/screens/SettingsScreen'
import PlanningScreen from 'components/screens/PlanningScreen';
const RCTNetworking = require('react-native/Libraries/Network/RCTNetworking')

class InApp extends Component {
  state = {
    index: 0,
    routes: [
      { key: 'alarms', title: 'Réveils', icon: 'alarm' },
      { key: 'planning', title: 'Emploi du temps', icon: 'calendar' },
      { key: 'settings', title: 'Paramètres', icon: 'settings' }
    ],
  };
  _handleIndexChange = index => this.setState({ index });

  _renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'alarms':
        return <AlarmsScreen style={styles.container} jumpTo={jumpTo} />;
      case 'settings':
        return <SettingsScreen jumpTo={jumpTo} />;
      case 'planning':
        return <PlanningScreen jumpTo={jumpTo} />;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Appbar style={{ Top: 0, backgroundColor: '#0e3b5a' }}>
          <Appbar.Content title="MyEfreveil" />
          <Appbar.Action
            icon="power-standby"
            onPress={() => {
              RCTNetworking.clearCookies(() => { })
              this.props.setIsLogged()
            }}
          />
        </Appbar>
        <BottomNavigation
          navigationState={this.state}
          onIndexChange={this._handleIndexChange}
          renderScene={this._renderScene}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default InApp;