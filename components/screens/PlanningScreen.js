import React, { Component } from 'react';
import { Text } from 'react-native-paper';
import { View } from 'react-native';
import { parser } from '../../helpers/parser';
import { stringtoHours } from "../../helpers/stringtoHours";
import { StyleSheet } from "react-native";
import { Table, TableWrapper, Row, Rows, Col} from 'react-native-table-component';

class PlanningScreen extends Component {
  state = {
    planning: [],
    jours: ['','Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    tableTitle: ['Cours', 'H', 'Salle'],
    
  
}

async componentDidMount() {
  var parsed_planning = await parser();
  this.setState({ planning: parsed_planning })
  

}

setTableData(planning){
  var matiere=[], elem1=[], elem2=[], elem3=[];
  planning.map((day)=>{
    if(day!=null){
      elem1.push(day.prgoOfferingDesc)
      elem2.push(stringtoHours(day.timeCrTimeFrom))
      elem3.push(day.srvTimeCrDelRoom)
    }
  })
  matiere.push(elem1)
  matiere.push(elem2)
  matiere.push(elem3)
  return matiere;
}


render() {
  var tab = this.setTableData(this.state.planning)
  //console.log(tab)
  return (
    <View style={styles.container}>
        <Table borderStyle={{borderWidth: 1, borderColor: '#c8e1ff'}}>
          <Row data={this.state.jours} flexArr={[1, 2, 2, 2,2,2,2]} style={styles.head} textStyle={styles.text}/>
          <TableWrapper style={styles.wrapper}>
            <Col data={this.state.tableTitle} style={styles.col}  textStyle={styles.text}/>
            <Rows  data={tab} flexArr={[2, 2, 2,2,2,2]} style={styles.row} textStyle={styles.text}/>
          </TableWrapper>
        </Table>
      </View>
  )
}
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff', justifyContent:'center' },
  head: {  height: 40,  backgroundColor: '#7192ED'  },
  wrapper: { flexDirection: 'row' },
  title: { flex: 1, backgroundColor: '#f6f8fa' },
  row: {  height: 145, backgroundColor:'#E8F1FA'},
  col:{backgroundColor: '#D0DCFE'},
  text: { textAlign: 'center', fontSize:10, fontWeight:"100"  }
});

export default PlanningScreen;