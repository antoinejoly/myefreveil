import React, { Component } from 'react';
import { StyleSheet } from "react-native";
import { parser } from 'helpers/parser';
import { update_notifications } from 'helpers/updateNotifications';
import { List, Switch } from 'react-native-paper';
import { stringtoHours } from "helpers/stringtoHours";
import AsyncStorage from '@react-native-async-storage/async-storage';

class AlarmsScreen extends Component {

  state = {
    planning: [],
    alarms_status: [true, true, true, true, true, true, true]
  }

  async componentDidMount() {
    var parsed_planning = await parser();
    await AsyncStorage.setItem("@planning", JSON.stringify(parsed_planning))
    update_notifications()
    this.setState({ planning: parsed_planning })
    var storage = await AsyncStorage.getItem('@alarms')
    if (storage != null) {
      this.setState({ alarms_status: JSON.parse(storage) })
    }
  }

  changeStatus = async (index) => {
    var tmpdata = this.state.alarms_status
    tmpdata[index] = !tmpdata[index]
    this.setState({ alarms_status: tmpdata })
    await AsyncStorage.setItem('@alarms', JSON.stringify(tmpdata))
  }

  render() {
    var jours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    return (
      this.state.planning.map((day) => {
        var pos_index = this.state.planning.indexOf(day)
        if (day != null) {
          return <List.Item
            style={styles.list}
            title={jours[new Date(day.srvTimeCrDateFrom).getDay()] + " à " + stringtoHours(day.timeCrTimeFrom)}
            description={day.prgoOfferingDesc + ' - ' + day.srvTimeCrActivityId}
            right={props =>
              <Switch
                color={'#0e3b5a'}
                onValueChange={() => console.log('ok')}
                value={this.state.alarms_status[pos_index]}
                onValueChange={() => this.changeStatus(pos_index)}
              />
            }
            onPress={() => this.changeStatus(pos_index)}
            key={day.srvTimeCrDateFrom}
          />
        }
      })
    );
  }
}

const styles = StyleSheet.create({
  list: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(85, 85, 85, .05)',
  }
});
export default AlarmsScreen;