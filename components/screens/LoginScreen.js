import React, { Component } from 'react';
import { WebView } from 'react-native-webview';

class LoginScreen extends Component {

  onMessage = async (event) => {
    const { url } = event.nativeEvent;
    if(url=="https://www.myefrei.fr/portal/student/home") {
      this.props.setIsLogged()
    }
  };

  CHECK_URL = `ReactNativeWebView.postMessage(document.url); true;`;  

  render() {
    return (
      <WebView
        source={{ uri: 'https://www.myefrei.fr/portal/student/home' }}
        pullToRefreshEnabled={true}
        sharedCookiesEnabled={true}
        injectedJavaScript={this.CHECK_URL}
        onMessage={this.onMessage}
        allowsBackForwardNavigationGestures={true}
      />
    );
  }
}

export default LoginScreen;
