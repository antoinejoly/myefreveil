import React, { Component } from 'react';
import { IconButton } from 'react-native-paper';

class LogoutButton extends Component {
    render() {
      return (
          <IconButton
          icon="power-standby"
          size={20}
          onPress={this.props.setIsLogged(false)}
        />
      );
    }
  }
export default LogoutButton;
