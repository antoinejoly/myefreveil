import * as Notifications from 'expo-notifications';
import { Audio } from 'expo-av';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { stringtoHours } from "helpers/stringtoHours";

export async function notifications(data) {

    var offset = new Date(JSON.parse(await AsyncStorage.getItem('@offset')))
    if(offset) {
        offset = offset.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'})
    } else {
        offset = "00:30"
    }
    
    var hours_course=stringtoHours(data.timeCrTimeFrom)
    var date_course = new Date(new Date(data.srvTimeCrDateFrom).setHours(hours_course.split(':')[0],hours_course.split(':')[1],0))
    date_course.setHours(date_course.getHours()-offset.split(':')[0])
    date_course.setMinutes(date_course.getMinutes()-offset.split(':')[1])
    var time_ring = ((date_course.getTime() - new Date().getTime())/1000)+3600

    var { sound } = await Audio.Sound.createAsync(
        require('res/music.mp3')
     );

    Notifications.addNotificationReceivedListener(async notification => {
        Audio.setAudioModeAsync({
            staysActiveInBackground: true
        })

         await sound.setIsLoopingAsync(true)
         await sound.playAsync(); 
    });

    Notifications.addNotificationResponseReceivedListener(async event => (
        await sound.unloadAsync(),
        await sound.stopAsync()
    ))

    Notifications.setNotificationHandler({
        handleNotification: async () => ({
            shouldShowAlert: true,
            shouldSetBadge: true,
        }),
    });

    // Prepare the notification channel
    Notifications.setNotificationChannelAsync('alarms', {
        name: 'Réveils notifications',
        importance: Notifications.AndroidImportance.HIGH,
    });

    Notifications.scheduleNotificationAsync({
        content: {
            title: "MyEfreveil - Vous avez bientôt cours !",
            body: "Appuyer sur cette notification pour arrêter le réveil",
            sticky: true,
        },
        trigger: {
            seconds: time_ring,
            channelId: 'alarms', // <- for Android 8.0+, see definition above
        },
    });
}
