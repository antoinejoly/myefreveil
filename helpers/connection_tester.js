import { getWeek } from './time'

export async function connection_tester() {
    var { firstday, lastday } = getWeek();
    var output = await fetch("https://myefrei.fr/api/extranet/student/queries/planning?startdate=" + firstday + "&enddate=" + lastday)
        .then((res) => {
            if (res['url'] == "https://auth.myefrei.fr/uaa/login") {
                return false
            } else {
                return true
            }
        })
    return output
}