export function stringtoHours(string) {
    if(string.length==3) {
        return "0"+string.slice(0,1)+":"+string.slice(1, 3);
    } else {
        return string.slice(0,2)+":"+string.slice(2, 4);
    }
}