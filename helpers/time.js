export function getWeek() {
    var curr = new Date; // get current date
    var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    var last = first + 6; // last day is the first day + 6
    var firstday = new Date(curr.setDate(first)).toISOString().split('T')[0];
    var lastday = new Date(curr.setDate(last)).toISOString().split('T')[0];
    return {firstday, lastday}
}

export function addDay(date) {
    date = new Date(date)
    var copy = date
    copy.setDate(date.getDate() + 1)
    return copy.toISOString().split('T')[0]
}