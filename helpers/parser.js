import { getWeek, addDay } from 'helpers/time'
import { Alert } from "react-native";

export async function parser() {
    var { firstday, lastday } = getWeek();
    var url = "https://myefrei.fr/api/extranet/student/queries/planning?startdate=" + firstday + "&enddate=" + lastday
    return fetch(url)
        .then((res) => res.json())
        .then((res) => {
            var weeklyplanning = []
            var current = firstday;
            for (var i in res['rows']) {
                var date = res['rows'][i]['srvTimeCrDateFrom'].split("T")[0]
                if (date > current) {
                    current = date;
                    weeklyplanning.push(null)
                }
                if (current == date) {
                    weeklyplanning.push(res['rows'][i])
                    current = addDay(current);
                }
            }
            return weeklyplanning;
        })
        .catch(() =>
            Alert.alert(
                'Session expirée',
                "Veuillez vous reconnecter"
            )
        )
}