import * as Notifications from 'expo-notifications';
import { notifications } from 'helpers/notifications'
import AsyncStorage from '@react-native-async-storage/async-storage';

export async function update_notifications() {
    Notifications.cancelAllScheduledNotificationsAsync()
    var disabledAlarms = JSON.parse(await AsyncStorage.getItem('@disabledAlarms'))
    if(!disabledAlarms) {
        var alarms = JSON.parse(await AsyncStorage.getItem('@alarms'))
        var planning = JSON.parse(await AsyncStorage.getItem('@planning'))
        if(alarms) {
            for(let i = 0; i<= alarms.length-1;i++) {
                if(alarms[i]==true && planning[i]!=null) {
                    notifications(planning[i])
                }
            }
        }
    }
}
