import * as BackgroundFetch from "expo-background-fetch"
import * as TaskManager from "expo-task-manager"
import { parser } from 'helpers/parser'

export async function push_update() {
    const TASK = "Mise à jour de votre emploi du temps"

    TaskManager.defineTask(TASK, async () => {
        try {
            // fetch data here...
            const receivedNewData = await parser()
            console.log("My task ", receivedNewData)
            return receivedNewData
                ? BackgroundFetch.Result.NewData
                : BackgroundFetch.Result.NoData
        } catch (err) {
            return BackgroundFetch.Result.Failed
        }
    })

    RegisterBackgroundTask = async () => {
        try {
            await BackgroundFetch.registerTaskAsync(TASK, {
                minimumInterval: 5, // seconds,
            })
            console.log("Task registered")
        } catch (err) {
            console.log("Task Register failed:", err)
        }
    }

}